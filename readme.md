Bitbull Translation Extension
=====================
Modulo per facilitare l'import delle traduzioni
Facts
-----
- version: 1.0.0
- extension key: Bitbull_Translation
- [extension on GitHub](https://github.com/bitbull/Bitbull_Translation)

Description
-----------
Module to make easy way to import translation

Contribution
------------
Any contribution is highly appreciated. The best way to contribute code is to open a [pull request on GitHub](https://help.github.com/articles/using-pull-requests).

Developer
---------
Bitbull

Licence
-------
[OSL - Open Software Licence 3.0](http://opensource.org/licenses/osl-3.0.php)

Copyright
---------
(c) 2015 Bitbull
