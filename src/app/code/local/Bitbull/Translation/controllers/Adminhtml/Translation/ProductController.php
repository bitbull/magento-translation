<?php
/**
 * @category Bitbull
 * @package  Bitbull_Translation
 * @author   Gennaro Vietri <gennaro.vietri@bitbull.it>
*/
class Bitbull_Translation_Adminhtml_Translation_ProductController extends Mage_Adminhtml_Controller_Action
{
    public function exportAction()
    {
        $storeViewId = $this->getRequest()->getParam('store', false);

        if ($storeViewId) {
            try {
                /** @var $exporter Bitbull_Translation_Model_Export_Product */
                $exporter = Mage::getModel('bitbull_translation/export_product');
                $exporter->setStore($storeViewId);

                return $this->_prepareDownloadResponse(
                    $exporter->getFileName(),
                    $exporter->export(),
                    $exporter->getContentType()
                );
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::logException($e);
                $this->_getSession()->addError($this->__('No valid data sent'));
            }
        } else {
            $this->_getSession()->addError($this->__('No valid data sent'));
        }

        return $this->_redirect('adminhtml/catalog_product/index');
    }

    public function importAction()
    {
        $storeViewId = $this->getRequest()->getParam('store', false);

        if ($storeViewId) {
            try {
                /** @var $importer Bitbull_Translation_Model_Import_Product */
                $importer = Mage::getModel('bitbull_translation/import_product');
                $importer->setStore($storeViewId);

                $importer->import($importer->uploadSource());

                $this->_getSession()->addSuccess($this->__('Traduzioni salvate con successo'));

            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::logException($e);
                $this->_getSession()->addError($this->__('No valid data sent'));
            }

            return $this->_redirect('adminhtml/catalog_product/index', array('store' => $storeViewId));
        } else {
            $this->_getSession()->addError($this->__('No valid data sent'));

            return $this->_redirect('adminhtml/catalog_product/index');
        }
    }
}