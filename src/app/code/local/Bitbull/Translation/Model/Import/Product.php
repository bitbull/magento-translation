<?php
/**
 * @category Bitbull
 * @package  Bitbull_Translation
 * @author   Gennaro Vietri <gennaro.vietri@bitbull.it>
*/
class Bitbull_Translation_Model_Import_Product extends Mage_Core_Model_Abstract
{
    const ENTITY = 'catalog_product';

    const FIELD_NAME_SOURCE_FILE = 'import_file';

    protected $_skuToIds = array();

    protected $_entityTypeId;

    /**
     * DB connection.
     *
     * @var Varien_Db_Adapter_Interface
     */
    protected $_connection;

    /**
     * Id della store view per cui esportare
     */
    protected $_storeId;

    public function __construct()
    {
        $entityType = Mage::getSingleton('eav/config')->getEntityType(self::ENTITY);
        $this->_entityTypeId    = $entityType->getEntityTypeId();
        $this->_connection      = Mage::getSingleton('core/resource')->getConnection('write');
    }

    public function setStore($storeId)
    {
        $this->_storeId = $storeId;

        return $this;
    }

    /**
     * Move uploaded file and create source adapter instance.
     *
     * @throws Mage_Core_Exception
     * @return string Source file path
     */
    public function uploadSource()
    {
        $uploader  = Mage::getModel('core/file_uploader', self::FIELD_NAME_SOURCE_FILE);
        $uploader->skipDbProcessing(true);
        $result    = $uploader->save(self::getWorkingDir());
        $extension = pathinfo($result['file'], PATHINFO_EXTENSION);

        $uploadedFile = $result['path'] . $result['file'];
        if (!$extension) {
            unlink($uploadedFile);
            Mage::throwException(Mage::helper('importexport')->__('Uploaded file has no extension'));
        }
        $sourceFile = self::getWorkingDir() . self::ENTITY;

        $sourceFile .= '.' . $extension;

        if(strtolower($uploadedFile) != strtolower($sourceFile)) {
            if (file_exists($sourceFile)) {
                unlink($sourceFile);
            }

            if (!@rename($uploadedFile, $sourceFile)) {
                Mage::throwException(Mage::helper('importexport')->__('Source file moving failed'));
            }
        }
        // trying to create source adapter for file and catch possible exception to be convinced in its adequacy
        try {
            $this->_getSourceAdapter($sourceFile);
        } catch (Exception $e) {
            unlink($sourceFile);
            Mage::throwException($e->getMessage());
        }
        return $sourceFile;
    }

    protected function _saveProductAttributes(array $attributesData)
    {
        foreach ($attributesData as $tableName => $skuData) {
            $tableData = array();

            foreach ($skuData as $sku => $attributes) {
                $productId = $this->_skuToIds[$sku];

                foreach ($attributes as $attributeId => $value) {
                    $tableData[] = array(
                        'entity_id'      => $productId,
                        'entity_type_id' => $this->_entityTypeId,
                        'attribute_id'   => $attributeId,
                        'store_id'       => $this->_storeId,
                        'value'          => $value
                    );
                }
            }
            $this->_connection->insertOnDuplicate($tableName, $tableData, array('value'));
        }
        return $this;
    }

    /**
     * Importa i dati dal file dato.
     *
     * @param string $sourceFile
    */
    public function import($sourceFile)
    {
        $this->_populateSkuToIdsMap();

        $attributes = array();

        $adapter = $this->_getSourceAdapter($sourceFile);
        foreach ($adapter as $row) {
            $sku = $row['sku'];
            unset($row['sku']);

            $attributes = $this->_prepareAttributes($row, $attributes, $sku);
        }

        $this->_saveProductAttributes($attributes);
    }

    /**
     * Import/Export working directory
     *
     * @return string
     */
    public static function getWorkingDir()
    {
        return Mage::getBaseDir('var') . DS . 'importexport' . DS;
    }

    /**
     * Returns source adapter object.
     *
     * @param string $sourceFile Full path to source file
     * @return Mage_ImportExport_Model_Import_Adapter_Abstract
     */
    protected function _getSourceAdapter($sourceFile)
    {
        return Mage_ImportExport_Model_Import_Adapter::findAdapterFor($sourceFile);
    }

    protected function _populateSkuToIdsMap()
    {
        $entityTable = Mage::getModel('importexport/import_proxy_product_resource')->getEntityTable();

        $newProducts = $this->_connection->fetchPairs($this->_connection->select()
                ->from($entityTable, array('sku', 'entity_id'))
        );
        foreach ($newProducts as $sku => $newId) {
            $this->_skuToIds[$sku] = $newId;
        }
    }

    /**
     * Retrieve attribute by specified code
     *
     * @param string $code
     * @return Mage_Eav_Model_Entity_Attribute_Abstract
     */
    protected function _getAttribute($code)
    {
        $attribute = Mage::getSingleton('importexport/import_proxy_product_resource')->getAttribute($code);
        $backendModelName = (string)Mage::getConfig()->getNode(
            'global/importexport/import/catalog_product/attributes/' . $attribute->getAttributeCode() . '/backend_model'
        );
        if (!empty($backendModelName)) {
            $attribute->setBackendModel($backendModelName);
        }
        return $attribute;
    }

    protected function _prepareAttributes($rowData, $attributes, $rowSku)
    {
        $product = Mage::getModel('importexport/import_proxy_product', $rowData);

        foreach ($rowData as $attrCode => $attrValue) {
            $attribute = $this->_getAttribute($attrCode);
            $attrId = $attribute->getId();
            $backModel = $attribute->getBackendModel();
            $attrTable = $attribute->getBackend()->getTable();

            if ('url_key' == $attribute->getAttributeCode()) {
                if (empty($attrValue)) {
                    $attrValue = $product->formatUrlKey($product->getName());
                }
            } elseif ($backModel) {
                $attribute->getBackend()->beforeSave($product);
                $attrValue = $product->getData($attribute->getAttributeCode());
            }

            if (!isset($attributes[$attrTable])) $attributes[$attrTable] = array();
            if (!isset($attributes[$attrTable][$rowSku])) $attributes[$attrTable][$rowSku] = array();

            $attributes[$attrTable][$rowSku][$attrId] = $attrValue;

            $attribute->setBackendModel($backModel); // restore 'backend_model' to avoid 'default' setting
        }
        return $attributes;
    }
}