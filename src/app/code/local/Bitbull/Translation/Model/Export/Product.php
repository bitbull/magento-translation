<?php
/**
 * @category Bitbull
 * @package  Bitbull_Translation
 * @author   Gennaro Vietri <gennaro.vietri@bitbull.it>
*/
class Bitbull_Translation_Model_Export_Product extends Mage_Core_Model_Abstract
{
    const ENTITY = 'products';

    /**
     * Id della store view per cui esportare
    */
    protected $_storeId;

    /**
     * @var Mage_ImportExport_Model_Export_Adapter_Csv
    */
    protected $_writer;

    public function setStore($storeId)
    {
        $this->_storeId = $storeId;

        return $this;
    }

    /**
     * Restuisce il csv con i prodotti per la storeview data
     *
     * @return string
    */
    public function export()
    {
        if (!$this->_storeId) {
            Mage::throwException('Impossibile esportare, store view non specificata');
        }

        // Elenco di attributi standard di Magento da non tradurre
        $excludeAttributes = array(
            'image_label',
            'old_id',
            'small_image_label',
            'thumbnail_label',
            'uf_product_link',
            'url_path',
        );

        $attributes = array('sku' => 'SKU');
        $headers = array('sku' => 'sku');

        // Recupero tutti gli attributi di tipo testo, cercando di escludere quelli di sistema
        $attributesCollection = Mage::getResourceModel('catalog/product_attribute_collection')
            ->addFieldToFilter('frontend_input', array('in' => array('textarea', 'text')))
            ->addFieldToFilter('backend_model', array('null' => true))
            ->addFieldToFilter('backend_type', array('neq' => 'static'))
            ->addFieldToFilter('attribute_code', array('nin' => $excludeAttributes))
        ;

        $productCollection = Mage::getModel('catalog/product')
            ->getCollection()
            ->addStoreFilter($this->_storeId)
            ->addAttributeToSelect('sku')
        ;

        foreach ($attributesCollection as $attribute) {
            $attributes[$attribute->getAttributeCode()] = $attribute->getFrontendLabel();
            $headers[$attribute->getAttributeCode()] = $attribute->getAttributeCode();

            $productCollection->addAttributeToSelect($attribute->getAttributeCode());

            // Occorre mettere in join esplicitamente gli attributi in base allo store id
            // addStoreFilter sulla collection sembra impattare solo l'associazione prodotto <-> website
            $productCollection->joinAttribute(
                $attribute->getAttributeCode(),
                'catalog_product/' . $attribute->getAttributeCode(),
                'entity_id',
                null,
                'left',
                $this->_storeId
            );
        }

        $this->_getWriter()->setHeaderCols($headers);

        foreach ($productCollection as $product) {
            $row = array();
            foreach ($attributes as $attributeCode => $attributeLabel) {
                $row[$attributeCode] = $product->getData($attributeCode);
            }

            $this->_getWriter()->writeRow($row);
        }

        return $this->_getWriter()->getContents();
    }

    /**
     * @return Mage_ImportExport_Model_Export_Adapter_Csv
    */
    protected function _getWriter()
    {
        if (!$this->_writer) {
            $this->_writer = Mage::getModel('importexport/export_adapter_csv', array());
        }

        return $this->_writer;
    }

    /**
     * MIME-type for 'Content-Type' header.
     *
     * @return string
     */
    public function getContentType()
    {
        return $this->_getWriter()->getContentType();
    }

    /**
     * Return file name for downloading.
     *
     * @return string
     */
    public function getFileName()
    {
        return self::ENTITY . '_' . Mage::app()->getStore($this->_storeId)->getName() . '_' . date('Ymd_His') .  '.' . $this->_getWriter()->getFileExtension();
    }
}